package org.jebtk.ui.modern.button;

import org.ebtk.event.ChangeEvent;
import org.ebtk.event.ChangeListener;
import org.jebtk.ui.modern.widget.ModernWidget;

public class DropDownButtonAnimation extends ButtonHighlightAnimation implements ChangeListener {
	public DropDownButtonAnimation(ModernWidget button) {
		super(button);
		
		((ModernDropDownWidget)button).addPopupClosedListener(this);
	}
	
	@Override
	public void animateMouseExited() {
		// If the popup is show, force the animation to display the button
		// by making it opaque and stopping the timer
		if (getButton().getPopupShown()) {
			opaque();
			stopMouseOverTimer();
		} else {
			super.animateMouseExited();
		}
	}
	
	@Override
	public void changed(ChangeEvent e) {
		pseudoMouseExited();
	}
}
