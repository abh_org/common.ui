package org.jebtk.ui.modern.button;

import java.awt.Graphics2D;

import org.jebtk.ui.modern.animation.WidgetAnimation;
import org.jebtk.ui.modern.theme.ModernWidgetRenderer;
import org.jebtk.ui.modern.widget.ModernClickWidget;
import org.jebtk.ui.modern.widget.ModernWidget;

public class CheckBoxSelectedAnimation extends WidgetAnimation {
	public CheckBoxSelectedAnimation(ModernWidget widget) {
		super((ModernClickWidget)widget);

		//setFadeColor("fill", ModernWidgetRenderer.SELECTED_FILL_COLOR);
	}

	@Override
	public void draw(ModernWidget widget, Graphics2D g2, Object... params) {
		int x = widget.getInsets().left;
		int y = (widget.getHeight() - ModernCheckBox.CHECKED_ICON.getWidth()) / 2;


		if (widget.isEnabled() && ((ModernClickWidget)getWidget()).isSelected()) {
			g2.setColor(ModernWidgetRenderer.SELECTED_FILL_COLOR); //getFadeColor("fill"));
			getWidget().getWidgetRenderer().fill(g2, x, y, ModernCheckBox.ICON_SIZE, ModernCheckBox.ICON_SIZE);
		}
	}
}
