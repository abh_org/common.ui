package org.jebtk.ui.modern.help;

import java.awt.Graphics2D;

import org.jebtk.ui.modern.button.ButtonHighlightAnimation;
import org.jebtk.ui.modern.graphics.ImageUtils;
import org.jebtk.ui.modern.graphics.icons.ModernMessageIcon;
import org.jebtk.ui.modern.ribbon.Ribbon;
import org.jebtk.ui.modern.ribbon.RibbonHighlightTextAnimation;
import org.jebtk.ui.modern.theme.RenderMode;
import org.jebtk.ui.modern.widget.ModernWidget;

public class HelpButtonHighlightAnimation extends ButtonHighlightAnimation {


	public HelpButtonHighlightAnimation(ModernWidget widget) {
		super(widget);

		setFadeColor("highlight", RibbonHighlightTextAnimation.HIGHLIGHT_COLOR);
	}

	@Override
	public void drawButton(Graphics2D g2, 
			int x,
			int y, 
			int w, 
			int h,
			RenderMode mode,
			boolean hasFocus) {

		int size = 20; //h - 2;

		int xf = ModernWidget.PADDING;
		int yf = y + (h - size) / 2;

		Graphics2D g2Temp = ImageUtils.createAAStrokeGraphics(g2);

		try {
			/*
			GradientPaint paint = new GradientPaint(0,
		             yf,
		             mColor1,
		             0,
		             yf + size,
		             mColor2);

			//g2Temp.setPaint(paint);
			 */

			g2Temp.setColor(getFadeColor("highlight"));
			g2Temp.fillOval(xf, yf, size, size);
			g2Temp.setColor(Ribbon.BAR_BACKGROUND);
			g2Temp.drawOval(xf, yf, size, size);

			ModernMessageIcon.drawScaledText(g2Temp, 
					size,
					xf,
					yf,
					size,
					size,
					"?",
					Ribbon.BAR_BACKGROUND);
		} finally {
			g2Temp.dispose();
		}
	}
}
