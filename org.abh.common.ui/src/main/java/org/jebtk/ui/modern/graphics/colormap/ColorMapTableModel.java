/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jebtk.ui.modern.graphics.colormap;

import java.util.Collections;
import java.util.List;

import org.ebtk.Mathematics;
import org.ebtk.collections.CollectionUtils;
import org.ebtk.event.ChangeEvent;
import org.ebtk.event.ChangeListener;
import org.ebtk.text.TextUtils;
import org.jebtk.ui.modern.table.ModernColumnHeaderTableModel;

// TODO: Auto-generated Javadoc
/**
 * Provides a view onto an excel workbook.
 * 
 * @author Antony Holmes Holmes
 *
 */
public class ColorMapTableModel extends ModernColumnHeaderTableModel implements ChangeListener {

	/**
	 * The constant HEADER.
	 */
	private static final String[] HEADER = {TextUtils.EMPTY_STRING, "Name"};
	
	/** The m names. */
	private List<String> mNames;
	
	/**
	 * Instantiates a new bed table model.
	 */
	public ColorMapTableModel() {
		ColorMapService.getInstance().addChangeListener(this);
		
		update();
	}
	
	/* (non-Javadoc)
	 * @see org.abh.common.ui.ui.dataview.ModernDataModel#getColumnAnnotations(int)
	 */
	@Override
	public List<String> getColumnAnnotationText(int column) {
		if (Mathematics.inBound(column, 0, HEADER.length)) {
			return CollectionUtils.toList(HEADER[column]);
		} else {
			return Collections.emptyList();
		}
	}
	
	/* (non-Javadoc)
	 * @see org.abh.common.ui.ui.dataview.ModernDataModel#getColumnCount()
	 */
	@Override
	public final int getColumnCount() {
		return HEADER.length;
	}

	/* (non-Javadoc)
	 * @see org.abh.common.ui.ui.dataview.ModernDataModel#getRowCount()
	 */
	@Override
	public final int getRowCount() {
		return mNames == null ? 0 : mNames.size();
	}
	
	/* (non-Javadoc)
	 * @see org.abh.common.ui.ui.dataview.ModernDataModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int row, int column) {
		switch (column) {
		case 0:
			return ColorMapService.getInstance().get(mNames.get(row));
		default:
			return mNames.get(row);
		}
	}

	/* (non-Javadoc)
	 * @see org.abh.common.event.ChangeListener#changed(org.abh.common.event.ChangeEvent)
	 */
	@Override
	public void changed(ChangeEvent e) {
		change();
	}
	
	/**
	 * Change.
	 */
	private void change() {
		update();
		
		fireDataChanged();
	}
	
	/**
	 * Update.
	 */
	private void update() {
		mNames = CollectionUtils.toList(ColorMapService.getInstance());
	}
}
