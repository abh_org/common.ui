package org.jebtk.ui.modern.dialog;

import org.jebtk.ui.modern.MaterialUtils;
import org.jebtk.ui.modern.button.ButtonHighlightAnimation;
import org.jebtk.ui.modern.widget.ModernWidget;

public class DialogMaterialButtonHighlightAnimation extends ButtonHighlightAnimation {
	public DialogMaterialButtonHighlightAnimation(ModernWidget button) {
		super(button);
		
		setFadeColor("fill", MaterialUtils.BUTTON_COLOR);
	}
}
