/**
 * Copyright 2017 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jebtk.ui.modern.animation;

import java.awt.Graphics2D;

import org.jebtk.ui.modern.widget.ModernWidget;

/**
 * An animation is a series of one or more widget drawings to indicate a 
 * sense of flow or movement.
 */
public interface Animation {
	
	
	
	/**
	 * The animation becomes responsible handling the drawing for the widget.
	 * This method this called on each animation refresh.
	 *
	 * @param widget the widget
	 * @param g2 the g 2
	 * @param params the params
	 */
	public void draw(ModernWidget widget, Graphics2D g2, Object... params);
}
